package DAO;

import FabricaDeSessoes.HibernateUtil;
import bean.Livro;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class DAOLivro {
    static SessionFactory fabrica = HibernateUtil.getSessionFactory();
    public void adicionarLivro(String autor, String titulo, int edicao, String isbn){
        Livro livro = new Livro();
        
        livro.setAutor(autor);
        livro.setTitulo(titulo);
        livro.setEdicao(edicao);
        livro.setIsbn(isbn);
        
        Session session = fabrica.openSession();
        session.beginTransaction();
        session.save(livro);
        session.getTransaction().commit();
        session.close();
        
        
    }
}
